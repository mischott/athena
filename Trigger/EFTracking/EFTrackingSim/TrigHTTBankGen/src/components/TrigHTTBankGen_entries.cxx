#include "../HTTMatrixGenAlgo.h"
#include "TrigHTTBankGen/HTTConstGenAlgo.h"
#include "../HTTMatrixReductionAlgo.h"
#include "../HTTMatrixMergeAlgo.h"

DECLARE_COMPONENT( HTTConstGenAlgo )
DECLARE_COMPONENT( HTTMatrixMergeAlgo )
DECLARE_COMPONENT( HTTMatrixReductionAlgo )
DECLARE_COMPONENT( HTTMatrixGenAlgo )
